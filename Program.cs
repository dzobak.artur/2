﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConsoleApp10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string fileName = "Student.xml";

            
            CreateXmlFile(fileName);

           
            DisplayXmlFile(fileName);

           
            string searchLastName = "Вогар";

           
            DisplayStudentInfoByLastName(fileName, searchLastName);

            
            string faculty = "Факультет ФМЦТ";
            int course = 3;
            CountExcellentMaleStudents(fileName, faculty, course);
        }

        
        static void CreateXmlFile(string fileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("Students");
            xmlDoc.AppendChild(root);

            string[] lastNames = { "Вогар", "Півкач", "Дзьобак" };
            string[] faculties = { "Факультет ФМЦТ", "Факультет ФІТ", "Факультет ФМЦТ" };
            int[] courses = { 1, 2, 3 };
            string[] genders = { "чоловік", "чоловік", "чоловік" };
            double[] scholarships = { 1000.0, 1200.0, 800.0 };
            double[][] grades = {
            new double[] { 90.5, 85.5, 88.0 },
            new double[] { 78.0, 88.5, 92.0 },
            new double[] { 95.0, 96.5, 91.0 }
        };

            for (int i = 0; i < lastNames.Length; i++)
            {
                XmlElement student = xmlDoc.CreateElement("Student");
                student.SetAttribute("LastName", lastNames[i]);
                student.SetAttribute("Faculty", faculties[i]);
                student.SetAttribute("Course", courses[i].ToString());
                student.SetAttribute("Gender", genders[i]);
                student.SetAttribute("Scholarship", scholarships[i].ToString());

                for (int j = 0; j < grades[i].Length; j++)
                {
                    XmlElement grade = xmlDoc.CreateElement($"Grade{j + 1}");
                    grade.InnerText = grades[i][j].ToString();
                    student.AppendChild(grade);
                }

                root.AppendChild(student);
            }

            xmlDoc.Save(fileName);
            Console.WriteLine("XML-файл створено: " + fileName);
        }

       
        static void DisplayXmlFile(string fileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            Console.WriteLine("Вміст XML-файлу:");

            Console.WriteLine(xmlDoc.OuterXml);
        }

        
        static void DisplayStudentInfoByLastName(string fileName, string lastName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);

            XmlNodeList students = xmlDoc.SelectNodes("//Student[@LastName='" + lastName + "']");
            if (students.Count > 0)
            {
                foreach (XmlNode student in students)
                {
                    Console.WriteLine("Прізвище: " + student.Attributes["LastName"].Value);
                    Console.WriteLine("Факультет: " + student.Attributes["Faculty"].Value);
                    Console.WriteLine("Курс: " + student.Attributes["Course"].Value);
                    Console.WriteLine("Стать: " + student.Attributes["Gender"].Value);
                    Console.WriteLine("Студентська стипендія: " + student.Attributes["Scholarship"].Value);

                    foreach (XmlNode grade in student.ChildNodes)
                    {
                        Console.WriteLine("Оцінка: " + grade.InnerText);
                    }
                }
            }
            else
            {
                Console.WriteLine("Студента з прізвищем " + lastName + " не знайдено.");
            }
        }

       
        static void CountExcellentMaleStudents(string fileName, string faculty, int course)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);

            XmlNodeList students = xmlDoc.SelectNodes($"//Student[@Faculty='{faculty}' and @Course='{course}' and @Gender='чоловік']");
            int excellentCount = 0;

            foreach (XmlNode student in students)
            {
                bool isExcellent = true;

                foreach (XmlNode grade in student.ChildNodes)
                {
                    double gradeValue = double.Parse(grade.InnerText);
                    if (gradeValue < 90.0)
                    {
                        isExcellent = false;
                        break;
                    }
                }

                if (isExcellent)
                {
                    excellentCount++;
                }
            }

            Console.WriteLine($"Кількість хлопців-відмінників на {course}-му курсі факультету {faculty}: {excellentCount}");
        }
    }
}
